import { combineReducers } from 'redux'

const heroes = (state = [], action) => {
  switch (action.type) {
    case 'ADD_HEROES':
      return [
        ...state,
        ...action.heroes,
      ]
    default:
      return state
  }
}

export default combineReducers({heroes})