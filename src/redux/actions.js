export const addHeroes = heroes => ({
  type: 'ADD_HEROES',
  heroes
})