import React from 'react'

import './List.css'

const List = ({title, items}) => {
  return (
    <>
      {items.length > 0 &&
        <div className="List"> 
          <h4>{title}</h4>
          {items.map(item => (
            <div className="ListItem">
              <a href={item.resourceURI}>{item.name}</a>
            </div>
          ))}
        </div>
      }
    </>
  )
}

export default List