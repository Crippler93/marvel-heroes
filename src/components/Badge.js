import React from 'react'

import './Badge.css'

const Badge = ({text, number, color="black", colorText="White"}) => {
  return (
    <div className="Badge" style={{backgroundColor: color}}>
      <span style={{color: colorText}}>{text} {number}</span>
    </div>
  )
}

export default Badge