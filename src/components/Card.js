import React from 'react'

import Badge from './Badge';

import "./Card.css";

const Card = ({heroe, onClick}) => {
  const {id, name, thumbnail, comics: {returned: totalComics}, series: {returned: totalSeries}, stories: {returned: totalStories}, events: {returned: totalEvents} } = heroe
  const urlImg = `${thumbnail.path}.${thumbnail.extension}`

  const handleClick = () => {
    onClick(id);
  }

  return (
    <div className="Card" onClick={handleClick}>
      <img className="CardImage" src={urlImg} alt={name}></img>
      <div className="CartContent">
        <span className="CardTitle">{name}</span>
        <div className="CardBadges">
          <Badge number={totalComics} text="comics" color="#C70039"></Badge>
          <Badge number={totalSeries} text="series" color="#571845"></Badge>
          <Badge number={totalStories} text="stories" color="#FF5733"></Badge>
          <Badge number={totalEvents} text="events"></Badge>
        </div>
      </div>
    </div>
  )
}

export default Card;