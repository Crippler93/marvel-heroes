import React, {useEffect, useState} from "react";

import {useParams} from "react-router-dom";

import { getHeroe } from "../service/marvel.service";
import List from "../components/List";

import './HeroesDetail.css'

const HeroesDetail = () => {
  let { id } = useParams();
  const [heroe, setHeroe] = useState({})

  const getImg = () => `${heroe.thumbnail.path}.${heroe.thumbnail.extension}`

  useEffect(() => {
    getHeroe(+id).then(res => setHeroe(res));
  }, [id])

  return (
    <>
      { heroe.id && (
        <div className="HeroesDetail">
          <h1 className="DetailTitle">{heroe.name}</h1>
          <div className="DetailDescription">
            <img className="DetailImage" src={getImg()} alt={heroe.name}></img>
            <p>{heroe.description}</p>
          </div>
          <div className="ListContainer">
            <List title="Comics" items={heroe.comics.items}></List>
            <List title="Series" items={heroe.series.items}></List>
            <List title="Stories" items={heroe.stories.items}></List>
            <List title="Events" items={heroe.events.items}></List>
          </div>
          <div className="DetailUrls">
            {heroe.urls.map((u, i) => (
              <div key={i} className="DetailUrl">
                <a href={u.url}>{u.type}</a>
              </div>
            ))}
          </div>
        </div>
      )}
    </>
  )
}

export default HeroesDetail