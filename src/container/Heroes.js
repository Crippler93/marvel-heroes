import React from "react";

import { useHistory } from "react-router-dom";
import { connect } from 'react-redux'

import Card from "../components/Card";

import './Heroes.css'

const Heroes = ({heroes}) => {

  let history = useHistory();

  const handleCardClick = (id) => {
    history.push(`/${id}`)
  }

  return (
    <div className="Heroes">
      <h3>Characters</h3>
      { heroes.map((heroe, i) =>
        <Card key={i} heroe={heroe} onClick={handleCardClick}></Card>
      )}
    </div>
  )
}

const mapStateToProps = state => {
  const heroes = state.heroes;
  return { heroes };
};

export default connect(mapStateToProps)(Heroes)