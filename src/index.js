import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from "react-redux";
import { createStore } from "redux";

import App from './App';
import * as serviceWorker from './serviceWorker';
import mainReducer from "./redux/reducer";

import './index.css';

const store = createStore(mainReducer)

ReactDOM.render(
  <Provider store={store}>  
    <App />
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
