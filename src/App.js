import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { connect } from 'react-redux';

import Heroes from './container/Heroes';
import HeroesDetail from './container/HeroesDetail';
import { getHeroes } from "./service/marvel.service";
import { addHeroes } from './redux/actions';

import './App.css';

function App({dispatch}) {

  getHeroes().then((res => {
    dispatch(addHeroes(res.data.data.results))
  }))

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          Marvel Heroes
        </header>
        <Heroes></Heroes>
        <Switch>
          <Route path="/:id" children={<HeroesDetail/>} />
        </Switch>
        
      </div>
    </Router>
  );
}

export default connect()(App);
