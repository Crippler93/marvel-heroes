import axios from "axios";
import getUrl from "./api";
const data = require("./test-data.json")

function get(url) {
  try {
    return axios.get(url)
  } catch (err) {
    console.error(err);
  }
}

export const getHeroes = () => {
  if(process.env.REACT_APP_MODE === 'dev') {
    return new Promise((res) => {
      res(data)
    })
  } else {
    return get(`https://gateway.marvel.com:443/v1/public/characters${getUrl()}`)
  }
}

export const getHeroe = (id) => {
  if(process.env.REACT_APP_MODE === 'dev') {
    const heroe = data.data.data.results.filter(heroe => heroe.id === id)[0]
    return new Promise((res) => {
      res(heroe)
    })
  } else {
    return get(`https://gateway.marvel.com:443/v1/public/characters/${id}${getUrl()}`)
  }
}