const md5 = require('md5');

function getHash(ts) {
  return md5(`${ts}${process.env.REACT_APP_PRIVATE_KEY}${process.env.REACT_APP_PUBLIC_KEY}`)
}

function getURL() {
  const ts = Date.now()
  const hash = getHash(ts)
  return `?ts=${ts}&apikey=${process.env.REACT_APP_PUBLIC_KEY}&hash=${hash}`
}

export default getURL